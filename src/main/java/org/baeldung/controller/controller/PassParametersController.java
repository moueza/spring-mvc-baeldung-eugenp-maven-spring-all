package org.baeldung.controller.controller;

import org.springframework.beans.factory.annotation.Autowired;

//import java.util.logging.Logger;

//import org.apache.commons.logging.impl.Log4JLogger;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.ModelAndView;

/**
 * In this controller, Model, ModelMap and ModelAndView are shown as examples.
 * They are all used to pass parameters to JSP pages. 04/09/2017
 *
 * @author Ahmet Cetin
 * 
 *         https://github.com/eugenp/tutorials/blob/1609a9a5de267c69f26dc80027424a74d3e37fe6/spring-all/src/main/java/org/baeldung/controller/controller/PassParametersController.java
 *         At Controller so no extends
 *         https://www.mkyong.com/spring-mvc/spring-mvc-hello-world-example/
 */
@Controller
public class PassParametersController {
	// Log4JLogger logger;
//	@Autowired
//	Person person;

	/** KO */
	@GetMapping("/do")
	public ModelAndView passParametersWithModelAndView3352() {
		ModelAndView modelAndView = new ModelAndView("do");
		// modelAndView.addObject("message", "message is Baeldung");
		System.out.println("in /do");
		return modelAndView;
	}

	/**** KO */
	@GetMapping("/string")
	public String passParametersWithModelMap4(ModelMap map) {
		System.out.println("in /string");
		return "string";
	}

	/** KO */
	@GetMapping("/dir/moi")
	public String passParametersWithModelMap2(ModelMap map) {
		// logger = new Log4JLogger("PassParametersController");
		// >logger.error("/dir/moi");
		System.out.println("dir moi");
		return "viewPage.jsp";
	}

	/** OK web.xml must have /moi, no wilcard * */
	@GetMapping("/moi")
	public ModelAndView passParametersWithModelAndView3() {
		ModelAndView modelAndView = new ModelAndView("viewPage");
		return modelAndView;
	}

	/** KO */
	@GetMapping("/do.do")
	public ModelAndView passParametersWithModelAndView332() {
		ModelAndView modelAndView = new ModelAndView("do");
		// modelAndView.addObject("message", "message is Baeldung");
		System.out.println("in do.do");
		return modelAndView;
	}

	/** separation from /do.do */
	@GetMapping("/action.action")
	public ModelAndView passParametersWithModelAndView3632() {
		ModelAndView modelAndView = new ModelAndView("viewPage");
		return modelAndView;
	}

	/** gen for general */
	@GetMapping("/dir/gen")
	public ModelAndView passParametersWithModelAndView33() {
		ModelAndView modelAndView = new ModelAndView("viewPage");
		return modelAndView;
	}

	/*** KO */
	@GetMapping("/dir/string")
	public String passParametersWithModel(Model model) {
		return "string";
	}

	@GetMapping("/printViewPage")
	public String passParametersWithModelMap(ModelMap map) {
		// map.addAttribute("welcomeMessage", "welcome");
		// map.addAttribute("message", "Baeldung");
		return "viewPage";
	}

	@GetMapping("/spring-mvc-baeldung-eugenp-maven-spring-all/dir/moi")
	public String passParametersWithModelMap3(ModelMap map) {

		return "viewPage";
	}

	@GetMapping("/dir/goToViewPage")
	public ModelAndView passParametersWithModelAndView() {
		ModelAndView modelAndView = new ModelAndView("viewPage");
		// modelAndView.addObject("message", "Baeldung");
		return modelAndView;
	}

	/** KO */
	@GetMapping("/dir/go")
	public ModelAndView passParametersWithModelAndView5() {
		ModelAndView modelAndView = new ModelAndView("viewPage.jsp");
		// modelAndView.addObject("message", "Baeldung");
		return modelAndView;
	}

	/** KO */
	@GetMapping("/dir/go2")
	public ModelAndView passParametersWithModelAndView6() {
		ModelAndView modelAndView = new ModelAndView("/pages/viewPage.jsp");
		// modelAndView.addObject("message", "Baeldung");
		return modelAndView;
	}

	@GetMapping("/spring-mvc-baeldung-eugenp-maven-spring-all/moi")
	public ModelAndView passParametersWithModelAndView2() {
		ModelAndView modelAndView = new ModelAndView("viewPage");

		return modelAndView;
	}

}